---
layout: post
title:  "Incrições abertas!!!"
date:   2021-01-01 00:00:00 -0300
image: '../img/post.jpg'
categories: incricao
published: true
---
A Universidade Tecnológica Federal do Paraná (UTFPR) e a Cisco Networking Academy (NetAcad) oferecem cursos em tecnologia da informação. Em Pato Branco a Academia Cisco é gerenciada pelo Departamento Acadêmico de Informática (DAINF). A UTFPR é a única instituição pública credenciada pela empresa no Sudoeste do Paraná.

A UTFPR Digital e Inclusiva está oferecendo duas modalidades de cursos gratuitos de forma remota. A primeira é de cursos on-line individual, em que o estudante é responsável por seguir a sequência do curso de forma individual, com a tutoria de um instrutor. A segunda é de cursos on-line, em que além de seguir a sequência do curso de forma individual, o estudante terá o apoio de vídeos disponibilizados semanalmente pelo instrutor, assim como uma aula semanal de forma remota.

[Clique aqui para mais detalhes][link-inscricao]

[link-inscricao]: http://www.pb.utfpr.edu.br/utfprdi/

