---
layout: page
image: /arduino/img/acendendo led.png
title: Acendendo um LED
toc: true
hide_hero: true
toc_title: Acendendo um LED
show_sidebar: false
---
## Objetivo
  Acender um LED. 

## Materiais utilizados

 - 1 LED vermelho
 - 1 LED amarelo
 - 1 LED verde
 - 1 LED azul
 - 4 Resistores 150 Ω
 - 6 Jumpers
 - 1 Arduino uno R3

 

## Esquema Arduino
![esquema arduino](/arduino/img/acendendo led.png)
  
  
## Código 
```
void setup()
{
  pinMode(2, OUTPUT);//define o pino 2 como saída
}

void loop()
{
  digitalWrite(2 ,HIGH);//acende o led que esta concectado ao pino 2
  delay(1000); // Espera por 1000 milisegundos(1 segundo)
  digitalWrite(2, LOW);//acende o led que esta concectado ao pino 2
  delay(1000); // Espera por 1000 milisegundos(1 segundo)
}
```
## Tutorial
<!-- blank line -->
<figure class="image is-16by9">
  <iframe class="has-ratio" src="https://www.youtube.com/embed/iiDbU0GhwIM"  frameborder="0" allowfullscreen=""> </iframe>
</figure>
<!-- blank line -->

## Simulador
<figure class="image is-16by9">
  <iframe class="has-ratio" src="https://www.tinkercad.com/embed/5Hnho3FgE8a?editbtn=1" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
  </figure>
