---
title: "Arduino: Conceitos, Programação e Aplicação Prática - UTFPR-Digital e Inclusiva"
layout: page
hero_height: is-fullheight
hero_darken: true
hero_image: img/arduino-1128227_1920.jpg
show_sidebar: true
---
# Visão geral do curso

O curso é uma introdução à plataforma Arduino, como programá-la e utilizá-la em projetos simples. Não é necessário ter o Arduino, pois será usado um ambiente virutal que permite testar todos os códigos e simular circuitos. No entanto, se o aluno tiver a possibilidade de adquirir o Arduino e os componentes, o aprendizado se torna ainda mais interessante.

# Benefícios

Ao concluir o curso o aluno conhecerá os diferentes tipos de Arduino, as possibilidades de desenvolvimento de projetos de eletrônica que o Arduino proporciona, assim como estará capacitado a fazer pequenos projetos envolvendo o Arduino.

# Certificação

Para aprovação e certificação, o aluno deverá ler todo o conteúdo do curso no ambiente educacional da Cisco, realizar todas as atividades que forem indicadas e realizar uma avaliação no final do curso.

# Metodologia

O curso é online, com material interativo contendo texto, imagens e vídeos. O curso é trabalhado em 10 semanas. Semanalmente são disponibilizados vídeos sobre os conteúdos, ao final de cada semana o aluno é desafiado a desenvolver um pequeno projeto com os assuntos aprendidos na semana.Haverá aulas remotas em que será demonstrado a resolução de projetos envolvendo os assuntos da semana, assim como tirar dúvidas em tempo real com os instrutores. O aluno deverá assistir os vídeos, ler os conteúdos do material e realizar as atividades propostas. Ao final do curso, será proposto um projeto envolvendo todo o conteúdo aprendido, como avaliação final. O aluno não precisa ter disponibilidade de estar presente nas aulas da sexta-feira à noite, pois as mesmas serão gravadas e ficarão disponíveis aos alunos, porém, é altamente recomendável a participação nas aulas remotas, visto que é o momento do aluno ser atendido em tempo real pelos instrutores. Todo o conteúdo estará disponível no ambiente virtual de aprendizagem moodle, da UTFPR.

## Componentes de aprendizagem

* Pequenos projetos semanais
* 1 projeto final
* Conteúdo interativo e educacional
* Aulas gravadas pelos instrutores
* Aulas remotas 
* Links para recursos relacionados

	
# Funcionalidades

* Pré-requisito: idealmente estar cursando ou ter cursado o ensino médio
* Nível: iniciante
* Duração estimada do curso: 40 horas