---
layout: page
title: Membros Do Projeto
show_sidebar: false
---
# UTFPR - Câmpus Pato Branco

## Coordenadores 
* Prof. Dr. Fabio favarim
* Prof. Dr. Jefesson José Lima

## Membros Atuais 
* [Matheus Henrique Xavier](https://xmatheusx.gitlab.io/)(Bolsista) 

# Membros Anteriors 

* Mateus (Bolsista)
* Willian (Voluntário)
* Bruno (Voluntário)
* teste (Voluntário)
* Willian (Voluntário)